This is a tool to decrypt strings from the stage1 binary (Browser.VC.db) of a North Koran campaign.  
For information purposes only.  

Browser.VC.db SHA256: 68e6b9d71c727545095ea6376940027b61734af5c710b2985a628131e47c6af7  

Reference: https://blog.google/threat-analysis-group/new-campaign-targeting-security-researchers/  

Sample Output:  
~$ py nk-campaign-deobfuscate.py  
Name: ValueName  
Length: 65  
Value: 6bt7cJNGEb3Bx9yK  
  
Name: Format  
Length: 57  
Value: C:\Windows\System32\rundll32.exe %s,%s %s %s  
  
Name: var_508  
Length: DB  
Value: ASN2_TYPE_new  
  
Name: var_4F8  
Length: 65  
Value: 5I9YjCZ0xlV45Ui8  
  
Name: PathName  
Length: 6A  
Value: C:\\ProgramData\\VirtualBox  
  
Name: FileName  
Length: 5F  
Value: C:\\ProgramData\\VirtualBox\\update.bin  
  
Name: SubKey  
Length: FD  
Value: SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\  
  
Name: ValueName2  
Length: DF  
Value: SSL Update  
  
