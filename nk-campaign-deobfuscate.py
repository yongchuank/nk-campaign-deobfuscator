"""
This is a tool to decrypt strings from the stage1 binary (Browser.VC.db) of a North Koran campaign.
For information purposes only.

Browser.VC.db SHA256: 68e6b9d71c727545095ea6376940027b61734af5c710b2985a628131e47c6af7

Reference: https://blog.google/threat-analysis-group/new-campaign-targeting-security-researchers/

(c) Yong Chuan Koh (@yongchuank) 2021, PixiePoint Security
"""

import os, sys, struct

def convert_str(mylist):
    output = ""
    for i in mylist:
        output += struct.pack("<L", i).decode("8859")
    return output

def deobfuscate(inbuff):
    inbuff = [ord(i) for i in list(inbuff)]
    keybuff = [ord(i) for i in list("\x6B\x49\xA3\x8D\xD8\xDD\x21\x2B\x38\x59\xBB\xBF\x06\xC0\x33\xC2")]
    runbuff = [ord(i) for i in list("\x00" * 0x100)]
    
    for i in range(0x100):
        runbuff[i] = i

    v6 = 0
    for i in range(0x100):
        v8 = i % 16
        v6 = (v6 + runbuff[i] + keybuff[v8]) & 0x00FF
        v9 = runbuff[i]
        result = runbuff[v6]
        runbuff[i] = result
        runbuff[v6] = v9

    v19 = 0
    v12 = (v19 & 0xFF00) >> 8
    v13 = v19
    inbuffsize = inbuff[0] + 1
    for i in range(1, inbuffsize):  #first byte = size of inbuff

        v19 = (v19 & 0xFF00)  | ((v13 + 1) & 0x00FF)
        v15 = (v13 + 1) & 0x00FF
        v19 = (((runbuff[v15] + v12) << 8) & 0xFF00) | (v19 & 0x00FF)
        v16 = runbuff[v15]
        v17 = (v19 & 0xFF00) >> 8
        runbuff[v15] = runbuff[(v19 & 0xFF00) >> 8]
        runbuff[v17] = v16
        v12 = (v19 & 0xFF00) >> 8
        v13 = v19
        result = (runbuff[v19 & 0x00FF] + runbuff[(v19 & 0xFF00) >> 8]) & 0x00FF
        inbuff[i-1] = inbuff[i] ^ runbuff[result]
   
    return (result, "".join(["%c"%i for i in inbuff[0 : inbuffsize - 1]]))

ObfuscatedStrings = {
    "ValueName": [0x71C6D310, 0x90E70381, 0xF33ED55B, 0x12AA8F16, 0x0000A3E3],
    "Format": [0x599EA62C, 0xBAC309E1, 0x9C2FE773, 0x1FE08E07, 0xCF0508CD, 0x4105489C, 0x30D7F398, 0xB8DB37B5, 0x27A263DB, 0x244D1746, 0x16200CAF, 0x0000A2B7],
    "var_508": [0x4BF7A40D, 0x87F93F84, 0xAE03D54C, 0x8031, 0x00A5],
    "var_4F8": [0x3CEDD010, 0x84EE0AEF, 0x9630E82C, 0x02C6C260, 0x0000AA90],
    "PathName": [0x599EA61B, 0xB1DF30EA, 0xAD3DE27B, 0x0AE79610, 0x946039F4, 0x4E054EB2, 0x7BD4DD90, 0x0000001A, 0x8DA3496B, 0x2B21DDD8, 0xBFBB5938, 0xC233C006],
    "FileName": [0x599EA627, 0xB1DF30EA, 0xAD3DE27B, 0x0AE79610, 0x946039F4, 0x4E054EB2, 0x7BD4DD90, 0xB0CB45DB, 0x31F322DA, 0x6A575044, 0x000000E3],
    "SubKey": [0x43EBB633, 0x8CEC37E2, 0x8D00CC59, 0x4E1943D, 0x89500ADB, 0x4627669C, 0x74D4FB92, 0x83E245F4, 0x31F531CB, 0x61684604, 0x5C690CF8, 0x0B08A96AA, 0x0BE4BDF40, 0x33],
    "ValueName2": [0x49F7B60A, 0x0BADD3596, 0x0A39E47D],
    }

for ObfuscatedStringName, ObfuscatedString in ObfuscatedStrings.items():
    DeobfuscatedResult, DeobfuscatedString = deobfuscate(convert_str(ObfuscatedString))
    print("Name: %s"%(ObfuscatedStringName))
    print("Length: %02X"%(DeobfuscatedResult))
    print("Value: %s"%(DeobfuscatedString))
    print("")
